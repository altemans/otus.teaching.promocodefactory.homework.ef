﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        public readonly DataContext _dataContext;
        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public void InitializeDb()
        {
            //_dataContext.Database.EnsureDeleted();
            //_dataContext.Database.EnsureCreated();

            _dataContext.Roles.AddRange(FakeDataFactory.Roles);
            _dataContext.Employees.AddRange(FakeDataFactory.Employees);

            _dataContext.PromoCodes.AddRange(FakeDataFactory.PromoCodes);
            _dataContext.Preferences.AddRange(FakeDataFactory.Preferences);

            _dataContext.Customers.AddRange(FakeDataFactory.Customers);

            _dataContext.SaveChanges();

        }


    }
}
