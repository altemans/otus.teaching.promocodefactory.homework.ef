﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPromoCodes : BaseEntity
    {
        public Guid CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        public Guid PromoCodeId { get; set; }
        [ForeignKey("PromoCodeId")]
        public PromoCode PromoCode { get; set; }
    }
}
