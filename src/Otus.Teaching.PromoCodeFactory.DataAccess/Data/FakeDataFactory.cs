﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                //Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                RoleId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                //Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                RoleId = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                AppliedPromocodesCount = 10
            },
            new Employee()
            {
                Id = Guid.Parse("f6cb9050-1073-4006-80fb-367567e25754"),
                Email = "avdeev@somemail.ru",
                FirstName = "Кирилл",
                LastName = "Авдеев",
                //Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                RoleId = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                AppliedPromocodesCount = 10
            },
            new Employee()
            {
                Id = Guid.Parse("dd6bef8b-4ed6-4439-9b83-83537d5aa0b2"),
                Email = "vlasov@somemail.ru",
                FirstName = "Илья",
                LastName = "Власов",
                //Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                RoleId = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        CustomerPreferences = new List<CustomerPreference>()
                        {
                           new CustomerPreference()
                           {
                               Id = Guid.Parse("4c385dbb-47da-4c93-b281-8d162dd89c57"),
                               CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                               PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                           },
                        },
                        CustomerPromoCodes = new List<CustomerPromoCodes>()
                        {
                            new CustomerPromoCodes()
                            {
                                Id = Guid.Parse("a5b72dd6-f8f2-47d0-a994-bbf54d4de07f"),
                                CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                                PromoCodeId = Guid.Parse("1b057e8f-3b1e-498c-b057-ea25c23cab54"),
                            }
                        },
                        //TODO: Добавить предзаполненный список предпочтений
                        //Preferences = Preferences.Where(x => x.Name == "Семья").ToList(),
                        //PromoCodes = PromoCodes.Where(x => Preferences.Where(n => n.Name == "Семья").ToList().Contains(x.Preference)).ToList()
                    },
                    new Customer()
                    {
                        Id = Guid.Parse("991437b8-307b-458f-9ff7-71d93f0cbce0"),
                        Email = "vasiliy_ovchannikov@mail.ru",
                        FirstName = "Василий",
                        LastName = "Овчинников",
                        CustomerPreferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference()
                            {
                                Id = Guid.Parse("8021714b-7857-4d0f-b508-654a6f97b425"),
                                CustomerId = Guid.Parse("991437b8-307b-458f-9ff7-71d93f0cbce0"),
                                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                            }
                        },
                        CustomerPromoCodes = new List<CustomerPromoCodes>()
                        {
                            new CustomerPromoCodes()
                            {
                                Id = Guid.Parse("1186c5e8-d7ff-47f7-9d9e-aaab6edc783b"),
                                CustomerId = Guid.Parse("991437b8-307b-458f-9ff7-71d93f0cbce0"),
                                PromoCodeId = Guid.Parse("cfd7103d-3d7a-4211-aa6a-0c53e5610f26"),
                            }
                        },
                        //Preferences = Preferences.Where(x => x.Name == "Дети").ToList(),
                        //PromoCodes = PromoCodes.Where(x => Preferences.Where(n => n.Name == "Дети").ToList().Contains(x.Preference)).ToList()
                    }
                };
        
                return customers;
            }
        }

        public static IEnumerable<PromoCode> PromoCodes
        {
            get
            {
                var promoCodes = new List<PromoCode>()
                {
                    new PromoCode()
                    {
                        Id = Guid.Parse("cfd7103d-3d7a-4211-aa6a-0c53e5610f26"),
                        Code = "1051",
                        ServiceInfo = "Two children = x2 on 10 days",
                        BeginDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(10),
                        PartnerName = "Страна детей",
                        //PartnerManager = Employees.FirstOrDefault(x => x.Email == "andreev@somemail.ru"),
                        //Preference = Preferences.FirstOrDefault(x => x.Name == "Дети")
                        PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                        PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("1b057e8f-3b1e-498c-b057-ea25c23cab54"),
                        Code = "1114",
                        ServiceInfo = "30% discount for the whole family",
                        BeginDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(1),
                        PartnerName = "День семьи",
                        //PartnerManager = Employees.FirstOrDefault(x => x.Email == "vlasov@somemail.ru"),
                        //Preference = Preferences.FirstOrDefault(x => x.Name == "Семья"),
                        PartnerManagerId = Guid.Parse("dd6bef8b-4ed6-4439-9b83-83537d5aa0b2"),
                        PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("09317336-e8e8-45dd-8811-fb984eb2b303"),
                        Code = "1243",
                        ServiceInfo = "15% discount on going to the theater",
                        BeginDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(30),
                        PartnerName = "Искусство жизни",
                        //PartnerManager = Employees.FirstOrDefault(x => x.Email == "avdeev@somemail.ru"),
                        //Preference = Preferences.FirstOrDefault(x => x.Name == "Театр"),
                        PartnerManagerId = Guid.Parse("f6cb9050-1073-4006-80fb-367567e25754"),
                        PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                    },
                };
                return promoCodes;
            }
        }
    }
}