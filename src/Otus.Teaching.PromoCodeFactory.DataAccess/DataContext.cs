﻿using Microsoft.EntityFrameworkCore;

using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext()
        {

        }
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<CustomerPromoCodes> CustomerPromoCodes { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Customer>().Navigation(e => e.CustomerPreferences).AutoInclude();
            modelBuilder.Entity<Customer>().Navigation(e => e.CustomerPromoCodes).AutoInclude();
            modelBuilder.Entity<CustomerPreference>().Navigation(e => e.Preference).AutoInclude();
            modelBuilder.Entity<CustomerPromoCodes>().Navigation(e => e.PromoCode).AutoInclude();
            modelBuilder.Entity<PromoCode>().Navigation(e => e.Preference).AutoInclude();
        }

    }
}
