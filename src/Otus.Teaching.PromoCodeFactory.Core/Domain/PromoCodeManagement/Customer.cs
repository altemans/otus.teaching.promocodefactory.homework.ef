﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(100)]

        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(50)]
        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 
        //[InverseProperty("Id")]
        public List<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();
        //[InverseProperty("Id")]
        public List<CustomerPromoCodes> CustomerPromoCodes { get; set; } = new List<CustomerPromoCodes>();
        [MaxLength(50)]
        public string MiddleName { get; set; }
    }
}