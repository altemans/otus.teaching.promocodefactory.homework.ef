﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private IRepository<Customer> _customerRepository;
        private IRepository<CustomerPreference> _customerPreference;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<CustomerPreference> customerPreference)
        {
            _customerRepository = customerRepository;
            _customerPreference = customerPreference;

        }
        /// <summary>
        /// Получение всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            //throw new NotImplementedException();
            try 
            { 
                var customers = await _customerRepository.GetAllAsync();
                var shortCustomers = customers.Select(x => new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email
                }).ToList();
                return Ok(shortCustomers);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
        /// <summary>
        /// Получение клиента по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            //throw new NotImplementedException();
            try
            {
                var customer = await _customerRepository.GetByIdAsync(id);
                var resultCustomer = new CustomerResponse()
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email,
                    PromoCodes = customer.CustomerPromoCodes.Select(p => p.PromoCode).ToList().Select(c => new PromoCodeShortResponse()
                            {
                                Id = c.Id,
                                Code = c.Code,
                                ServiceInfo = c.ServiceInfo,
                                BeginDate = c.BeginDate.ToString(),
                                EndDate = c.EndDate.ToString(),
                                PartnerName = c.PartnerName
                            }).ToList(),
                    
                };
                return Ok(resultCustomer);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        /// <summary>
        /// Добавить нового клиента с предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            try
            { 
                var customer = await NewCustomer(request, new Customer());
                await _customerRepository.Create(customer);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
        /// <summary>
        /// Редактирование существующего клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            try
            {
                var customer = await _customerRepository.GetByIdAsync(id);
                var newCustomer = await NewCustomer(request, customer);
                await _customerRepository.Update(newCustomer);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        private async Task<Customer> NewCustomer(CreateOrEditCustomerRequest request, Customer customer)
        {
            var cp = await _customerPreference.GetAllAsync();
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.CustomerPreferences = cp.Where(x => request.PreferenceIds.Contains(x.Id)).ToList();
            return await Task.FromResult(customer);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            try
            {
                await _customerRepository.Delete(await _customerRepository.GetByIdAsync(id));
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}