﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IRepository<PromoCode> _promocodeReposiroty;
        private IRepository<Employee> _employeeReposiroty;
        private IRepository<Preference> _preferenceRepository;
        private IRepository<CustomerPreference> _customerPreferenceRepository;
        private IRepository<CustomerPromoCodes> _customerPromoCodesRepository;

        public PromocodesController(IRepository<PromoCode> promocodeReposiroty
            , IRepository<Preference> preferenceRepository, IRepository<Employee> employeeReposiroty
            , IRepository<CustomerPreference> customerPreferenceRepository
            ,  IRepository<CustomerPromoCodes> customerPromoCodesRepository)
        {
            _promocodeReposiroty = promocodeReposiroty;
            _preferenceRepository = preferenceRepository;
            _employeeReposiroty = employeeReposiroty;
            _customerPreferenceRepository = customerPreferenceRepository;
            _customerPromoCodesRepository = customerPromoCodesRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
                //TODO: Получить все промокоды 
            try
            { 
                var promocodes = await _promocodeReposiroty.GetAllAsync();
                var result = promocodes.Select(x => new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName,
                }).ToList();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            try
            {
                var preferences = await _preferenceRepository.GetAllAsync();
                var preference = preferences.FirstOrDefault(x => x.Name == request.Preference);
                var cprep = await _customerPreferenceRepository.GetAllAsync();

                var promocode = new PromoCode()
                {
                    Id = Guid.NewGuid(),
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(10),
                    Preference = preference,
                    PartnerManager = await _employeeReposiroty.GetByIdAsync(Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f")),
                    CustomersPromoCodes = cprep.Where(x => x.PreferenceId == preference.Id)
                    .Select(cpc => new CustomerPromoCodes()
                    {
                        Id = Guid.NewGuid(),
                        Customer = cpc.Customer,
                        CustomerId = cpc.CustomerId,
                    }).ToList(),
                };
                await _promocodeReposiroty.Create(promocode);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}